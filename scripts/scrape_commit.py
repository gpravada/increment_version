import sys
import gitlab
import argparse
import re



def get_project(gl, project_id):
    """ Function for getting the project object for a passed project_id

    Args:
        gl: Connected gitlab instance
        project_id: ID of the project to be acquired
    Returns: Project object

    """
    return gl.projects.get(project_id)


def get_commit(project, commit_hash):
    """ Function for getting a commit object for a passed commit hash

    Args:
        project: Project object for which you want to get a commit
        commit_hash: The git commit hash that you want to find

    Returns: The desired commit object

    """
    return project.commits.get(commit_hash)


def get_version_level_from_commit(commit):
    """ Attempts to scrape a commit description for a specified version level

    Args:
        commit: Commit to search

    Returns: Version level if found (major|minor|patch), None if not found

    """
    description = commit.message
    # The regex below searches for a white space followed by the words "major|minor|patch", ignoring case
    captures = re.findall(r"(?i)\s(major|minor|patch)(?!\S)", description)
    version_level = None
    # This ensures only one checkbox is selected. If more than one is selected, "None" is returned, and the version
    # level will default to "patch"
    if len(captures) == 1:
        version_level = captures[0].lower()
    elif len(captures) > 1:
        print("More than one version was selected. Defaulting to patch.")
    return version_level


def get_version_level_from_mr(mr):
    """ Attempts to scrape a merge request for a specified version level. This expects the default merge request
    template to be used, where version levels are specified by a label on the merge request.

    Args:
        mr: Merge request to scrape

    Returns: Version level if found (major|minor|patch), None if not found

    """
    if len(mr.labels) < 1:
        return None
    else:
        for label in mr.labels:
            captures = re.findall(r"VersionLevel::(patch|minor|major)", label)
            if len(captures) > 1:
                print("Multiple version level labels detected, defaulting to patch.")
                return None
            elif len(captures) == 1:
                return captures[0].lower()


def get_mr_from_commit(project, commit_hash):
    """ Function to get a merge object from a merge commit

    Args:
        project: Project object to get merge request from
        commit_hash: The git commit hash you want to find

    Returns: Merge request object

    """
    commit = project.commits.get(commit_hash)
    # Regular expression that searches for the Gitlab merge request syntax, e.g. !10 for merge request ID 10
    regex = "!(\d*)"
    matches = re.findall(regex, commit.message)
    mr = None
    if len(matches) == 1:
        mr_num = matches[0]
        mr = project.mergerequests.get(mr_num)
    return mr


def is_mr(commit):
    """ Basic function to search for the default merge message in a commit to identify it as a merge commit
    e.g. Looking for the text "Merge request | merge branch"

    Args:
        commit: Commit to be searched

    Returns: True if the regex is matched, false if not

    """
    if re.search(r"(?i)merge\srequest|merge\sbranch", commit.message):
        return True
    return False


def parse_args():
    """ Function for parsing passed args

    Returns: args dictionary, containing passed values

    """
    p = argparse.ArgumentParser()
    p.add_argument('URL', help='Base URL')
    p.add_argument('project_id', help='ID of gitlab project')
    p.add_argument('commit_hash', help='SHA-1 hash of git commit')
    p.add_argument('token', help='Gitlab access token')
    p.add_argument('branch', help='Which branch to increment the version of')
    p.add_argument('src_dir', help='Source directory to be used for updating version.py')

    args = p.parse_args()

    return args

def get_version_level(git_server: str, git_token: str, project_id: int, git_hash: str):
    print(git_server)
    print(git_token)
    print(project_id)
    print(git_hash)


    # gl = gitlab.Gitlab(git_server, private_token=git_token)

    # project = get_project(gl, project_id)
    # commit = get_commit(project, git_hash)
    # version_level = get_version_level_from_commit(commit)

    # if version_level is None and is_mr(commit):
    #     mr = get_mr_from_commit(project, git_hash)
    #     # Falls through to default level of patch if it can't find a MR
    #     if mr is None:
    #         print("Failed to find link to merge request in description.")
    #     else:
    #         version_level = get_version_level_from_mr(mr)
    #     if version_level is None:
    #         print("Unable to find version level, defaulting to 'patch'")
    #         version_level = 'patch'

    # return version_level
    return 'patch'

def main():
    """ Main function for scraping normal and merge commits for a specified version level for incrementing

    Returns: Return code status

    """
    args = parse_args()

    try:
        gl = gitlab.Gitlab(args.URL, private_token=args.token)

        project = get_project(gl, args.project_id)
        commit = get_commit(project, args.commit_hash)
        version_level = get_version_level_from_commit(commit)

        if version_level is None and is_mr(commit):
            mr = get_mr_from_commit(project, args.commit_hash)
            # Falls through to default level of patch if it can't find a MR
            if mr is None:
                print("Failed to find link to merge request in description.")
            else:
                version_level = get_version_level_from_mr(mr)
        if version_level is None:
            print("Unable to find version level, defaulting to 'patch'")
            version_level = 'patch'
        file_path = "{}/version.py".format(args.src_dir)
#        return_code = release.main(file_path, version_level, args.commit_hash, args.URL, args.token, args.project_id,
#                                   args.branch)
    except Exception as e:
        return_code = -1
        print("Exception encountered: {}".format(e))

    return return_code


if __name__ == '__main__':
    sys.exit(main())
